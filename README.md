SENG3400 - Assignment 3
=======================

### Authors ###

Tyler Haigh - C3182929

# Abstract #

This project aims to implement a CORBA client server application that generates a random number that is returned to a given client after a short delay. There are two client implementations: Deferred Synchronous and Asynchronous

# To Generate Stub code #

* Async Package -    `idlj  -fall -pkgPrefix syncApp async    sync.idl`
* Deferred Package - `idlj  -fall -pkgPrefix syncApp deferred sync.idl`
* Server Package -   `idlj  -fall -pkgPrefix syncApp server   sync.idl`

# To Compile the Application #

Run the following to compile each package

* Async Package -    `javac async/*.java`
* Deferred Package - `javac deferred/*.java`
* Server Package -   `javac server/*.java`

# To Run the Application #

The use of [ConEmu](https://conemu.github.io/) is recommended to allow monitoring of client-server interaction.

1. Configure your ConEmu window by partitioning it into three terminal windows

![ConEmu Partition](images/ConEmuPartition.png)

2. Start the ORB by running `orbd -ORBInitialPort 25565` in the left console
3. Start the Server by running `java server.SyncServer -ORBInitialHost localhost -ORBInitialPort 25565` in the center console
4. Start either of the clients by running one of the following in the right console:
    a.  `java async.SyncClient -ORBInitialHost localhost -ORBInitialPort 25565` for the Asyncronous Client
    a.  `java deferred.SyncClient -ORBInitialHost localhost -ORBInitialPort 25565` for the Deferred Client

After running the applications, you should have something like this:

![ConEmu Running](images/ConEmuRunning.png)


# Marker Notes #

This application has been compiled using `javac 1.8.0_40` and run using
```
java version "1.8.0_40"
Java(TM) SE Runtime Environment (build 1.8.0_40-b26)
Java HotSpot(TM) 64-Bit Server VM (build 25.40-b25, mixed mode)
```
