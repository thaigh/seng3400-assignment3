package deferred;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;
import deferred.syncApp.*;

/**
 * Client for the Deferred Implementation of the SyncApp
 *
 * @author Tyler Haigh - C3182929
 * @since 16/10/2015
 */
public class SyncClient
{
    private int counter = 0;
    private int value = 200;

    public static void main(String args[])
    {
        SyncClient client = new SyncClient();
        client.run(args);
    }

    /**
     * Runs the Deferred Client
     * @param args The console args to the program
     */
    private void run(String[] args) {
        try {

            // Create and initialize the ORB
            ORB orb = ORB.init(args, null);

            Sync syncImpl = getOrbHandle(orb);
            System.out.println("Obtained a handle on server object: " + syncImpl);

            Request req  = syncImpl._request("getRandomNumber");
            req.set_return_type(orb.get_primitive_tc(TCKind.tk_long));

            // Loop 5 times
            for(int i = 0; i < 5; i++) {
                System.out.format("Counter: %1$s Value: %2$s\n", counter, value);
                counter++;
                Thread.sleep(500);
            }

            // Call server
            System.out.println("Sending request to server");
            req.send_deferred();

            // Loop 5 times
            for(int i = 0; i < 5; i++) {
                System.out.format("Counter: %1$s Value: %2$s\n", counter, value);
                counter++;
                Thread.sleep(500);
            }

            // Resynchronise with server and get the value.
            // May (or more likely will) cause pause in execution
            req.get_response();
            value = req.return_value().extract_long();
            System.out.format("Received response from server: %1$s\n", value);

            // Loop 5 times
            for(int i = 0; i < 5; i++) {
                System.out.format("Counter: %1$s Value: %2$s\n", counter, value);
                counter++;
                Thread.sleep(500);
            }

            syncImpl._release();

        } catch (Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
    }

    /**
     * Gets a reference to the Sync Server Servant using the ORB
     * @param orb The Server ORB
     * @return A Servant to call API methods on against the server
     * @throws Exception Unable to resolve the names
     */
    private Sync getOrbHandle(ORB orb) throws Exception {

        // Get the root naming context
        org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");

        // Use NamingContextExt instead of NamingContext. This is
        // part of the Interoperable naming Service.
        NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

        // Resolve the Object Reference in Naming
        String name = "Sync";
        Sync syncImpl = SyncHelper.narrow(ncRef.resolve_str(name));

        return syncImpl;
    }
}