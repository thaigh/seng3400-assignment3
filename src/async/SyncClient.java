package async;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;
import async.syncApp.*;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

class SyncCallbackClient extends SyncCallbackPOA {

    private SyncClient client;
    public SyncCallbackClient(SyncClient client) {
        this.client = client;
    }

    @Override
    public void callback(int randomNumber) {
        client.setValue(randomNumber);
    }
}

/**
 * Client for the Asynchronous implementation for the SyncApp
 *
 * @author Tyler Haigh - C3182929
 * @since 16/10/2015
 */
public class SyncClient
{
    private int counter = 0;
    private int value = 200;
    private boolean responseReceived = false;
    private int iterationsLeft = 5;

    public void setValue(int value) {
        System.out.format("Received response from server: %1$s\n", value);
        this.value = value;
        this.responseReceived = true;
    }

    public static void main(String args[])
    {
        SyncClient client = new SyncClient();
        client.run(args);
    }

    /**
     * Runs the client application
     * @param args The args to the program
     */
    private void run(String[] args) {
        try {

            // Create and initialize the ORB
            ORB orb = ORB.init(args, null);

            Sync syncImpl = getOrbHandle(orb);
            SyncCallback callback = createCallBack(orb);
            System.out.println("Obtained a handle on server object: " + syncImpl);

            // Begin the main loop
            while (iterationsLeft > 0) {
                System.out.format("Counter: %1$s Value: %2$s\n", counter, value);

                // Call the server if the counter has hit 5
                if (counter == 5) {
                    System.out.println("Sending request to server");
                    syncImpl.getRandomNumberAsync(callback);
                }

                // Check if we need to exit
                if (responseReceived) {
                    iterationsLeft--;
                }

                counter++;

                Thread.sleep(500);
            }

            syncImpl._release();

        } catch (Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
    }

    private SyncCallback createCallBack(ORB orb) throws Exception {
        POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
        rootpoa.the_POAManager().activate();

        SyncCallbackClient callbackServant = new SyncCallbackClient(this);
        org.omg.CORBA.Object ref = rootpoa.servant_to_reference(callbackServant);
        return SyncCallbackHelper.narrow(ref);
    }

    /**
     * Gets a reference to the Sync Server Servant using the ORB
     * @param orb The Server ORB
     * @return A Servant to call API methods on against the server
     * @throws Exception Unable to resolve the names
     */
    private Sync getOrbHandle(ORB orb) throws Exception {

        // Get the root naming context
        org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");

        // Use NamingContextExt instead of NamingContext. This is
        // part of the Interoperable naming Service.
        NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

        // Resolve the Object Reference in Naming
        String name = "Sync";
        Sync syncImpl = SyncHelper.narrow(ncRef.resolve_str(name));

        return syncImpl;
    }
}