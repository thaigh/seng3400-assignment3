package server;

import org.omg.CORBA.ORB;
import server.syncApp.SyncCallback;
import server.syncApp.SyncPOA;

import java.util.Random;

/**
 * Server Servant Implementation for the SyncApp containing methods from the IDL
 *
 * @author - Tyler Haigh - C3182929
 * @since 16/10/2015
 */
class SyncImpl extends SyncPOA {
    private ORB orb;
    private static Random rand = new Random();
    private static final int MAX_SLEEP = 5 * 1000;
    private static final int MIN_SLEEP = 1 * 1000;

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    /**
     * Generates a random number and returns to the client after a short delay
     * @return A random number between 1 and 100
     */
    @Override
    public int getRandomNumber() {

        System.out.println("Received request to getRandomNumber");

        // Delay for a short amount of time
        int timeToSleep = MIN_SLEEP + rand.nextInt(MAX_SLEEP);
        try {  Thread.sleep(timeToSleep); }
        catch (InterruptedException e) { e.printStackTrace(); }

        System.out.format("Thread has woken up after %1$s milliseconds\n", timeToSleep);

        int val = 1 + rand.nextInt(100);
        System.out.format("Sending %1$s back to client\n", val);

        return val;
    }

    @Override
    public void getRandomNumberAsync(SyncCallback callRef) {
        int number = getRandomNumber();
        callRef.callback(number);
    }

}